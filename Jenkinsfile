pipeline {
    agent any
    environment {
        AUTHOR = 'Guillaume Rémy'
        PURPOSE    = 'This is a sample Django app'
        LIFE_QUOTE = 'The greatest glory in living lies not in never falling, but in rising every time we fall.'
    }
    stages {
        stage('Checkout') {
            steps {
                // Checkout your source code repository
                git branch: 'master',
                    url: 'https://gitlab.com/johnykessler/pthon-django-simple.git'
            }
        }
        stage('Setup') {
            steps {
                // Set up your virtual environment or any other dependencies
                sh 'python3 -m venv venv'
            }
        }
        stage('Install Dependencies') {
            steps {
                // Install required dependencies
                sh '. venv/bin/activate && pip3 install -r requirements.txt'
            }
        }
        stage('Test') {
            steps {
                // Run your Django tests
                sh '. venv/bin/activate && python3 manage.py test'
            }
        }
        stage('Quality') {
            environment {
                scannerHome = tool 'sonar-scanner'
            }
            steps{
                withSonarQubeEnv('sonar'){
                    sh "${scannerHome}/bin/sonar-scanner"
                }
            }
        }
        stage('Build and push') {
            environment {
                CREDS_JFROG = credentials('jfrog')
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                    )}"""
            }
            steps {
                // Build your Django application
                sh 'docker build -t artifactory.jonctions.com/docker/johnykessler/django-simple-app:${COMMIT_SHA} .'
                sh 'docker login -u "$CREDS_JFROG_USR" -p "$CREDS_JFROG_PSW" artifactory.jonctions.com'
                sh 'docker push artifactory.jonctions.com/docker/johnykessler/django-simple-app:${COMMIT_SHA}'
            }
        }
        stage('Deploy') {
            environment {
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                    )}"""
            }
            steps {
                // Deploy your Django application
                sh 'docker rm -f django-sample-app || true'
                sh 'docker run --name django-sample-app -d -p 8000:8000 -it -e AUTHOR -e PURPOSE -e LIFE_QUOTE artifactory.jonctions.com/docker/johnykessler/django-simple-app:${COMMIT_SHA}'
            }
        }
    }
}
